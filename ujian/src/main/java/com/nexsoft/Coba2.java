package com.nexsoft;

import java.util.Scanner;

// import java.util.Scanner;
public class Coba2 {
    public void tes3(){
        // Scanner inputUser = new Scanner(System.in);

        // System.out.print("Masukan tahun kalender : ");
        // System.out.println(inputUser);
        // int Year = inputUser.nextInt();

        int Year = 2020;  
        int startDayOfMonth = 4;
        int spaces = startDayOfMonth ;

        // data array bulan.
        String[] bulan = {
                "",                               // index ke-0 dibuat kosong agar dapat memulai dari index ke-1
                "January", "February", "March",
                "April", "May", "June",
                "July", "August", "September",
                "October", "November", "December"
            };
            
            // data array dari jumlah hari di setiap bulannya
            int[] tHari = {            // index ke-0 dibuat kosong agar dapat memulai dari index ke-1

                0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
            };

            for (int M = 1; M <= 12; M++) {// perulangan untuk bulan.

            // kondisi jika ada tahun kabisat, maka total hari akan manjadi 29 
            // (jujur bagian ini dari gugel codenya..)
            if  ((((Year % 4 == 0) && (Year % 100 != 0)) ||  (Year % 400 == 0)) && M == 2)
                tHari[M] = 29;


            // print kalendar header
            // Display dari bulan dan tahun yang dipilih.
            System.out.println("          "+ bulan[M] + " " + Year);


            System.out.println("_____________________________________");
            System.out.println("   Sun  Mon Tue   Wed Thu   Fri  Sat");

            // untuk mengatur spasi pada kalendar agar berurutan.s
                spaces = (tHari[M-1] + spaces)%7;// modulus 7 agar hari dapat dimulai dari sisa pembagian sebelumnya.

            // print 
            // for (int i = 0; i < spaces; i++)
            //     System.out.print("     ");
            for (int i = 1; i <= tHari[M]; i++) {
                System.out.printf(" %3d ", i);// 3 untuk mengatur jarak font pada layout
                if (((i + spaces) % 7 == 0) || (i == tHari[M])) System.out.println();
            } // jika index + space lalu dibagi modulus samadengan 0 
            // atau index samadengan total hari index [bulan], maka akan memberi enter.

            System.out.println(); // enter setelah hitungan jumlah hari dalam bulan tersebut habis.
        }

    }
}
    
